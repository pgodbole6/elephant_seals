#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "interpolation.h"
#include <iostream> 

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <iomanip>
using namespace std;

typedef vector <double> record_t;
typedef vector <record_t> data_t;
//-----------------------------------------------------------------------------
istream& operator >> ( istream& ins, record_t& record )
  {
  record.clear();
  string line;
  getline( ins, line );
  stringstream ss( line );
  string field;
  while (getline( ss, field, ' ' ))
    {

    stringstream fs( field );
    double f = 0.0;  // (default value is 0.0)
    fs >> f;

    record.push_back( f );
    }

  return ins;
  }

//-----------------------------------------------------------------------------

istream& operator >> ( istream& ins, data_t& data )
  {
  data.clear();

  record_t record;
  while (ins >> record)
    {
    data.push_back( record );
    }

  return ins;  
  }

//-----------------------------------------------------------------------------
// Now to put it all to use.

int main(int argc, char **argv)
{
  data_t data;

  ifstream infile( "../data/filtered_data_test.csv" );
  infile >> data;
  if (!infile.eof())
    {
    cout << "Fooey!\n";
    return 1;
    }
  infile.close();

    alglib::real_1d_array x;
    x.setcontent(data.size(),&data[0][0]);
    alglib::real_1d_array y;
    y.setcontent(data.size(),&data[0][4]);
   
    alglib::real_2d_array tbl;
    alglib::spline1dinterpolant s;

cout << data.size() << endl;
for (size_t itera = 0; itera < data.size(); itera++){
      x[itera] = data[itera][0] + data[itera][1];
//cout << setprecision(18)<< x[itera] << endl;
  }


for (size_t iterb=0; iterb<data.size(); iterb++){
	y[iterb] = data[iterb][4];
	//cout << setprecision(18)<< y[iterb] << endl;
	}


    // build spline

   alglib::ae_int_t n = 200;

   alglib::spline1dbuildmonotone(x, y, s);

   alglib::spline1dunpack(s, n, tbl);


int width = 5, height = n-2;

 for (int iterc = 0; iterc < height; iterc++)
    {
        for (int j = 0; j < width; j++)
        {
           cout << tbl[iterc][j] << ' ';
        }
        cout << endl;
    }

    return 0;
}
