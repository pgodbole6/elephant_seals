#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>      // std::setprecision
#include <cmath>

using namespace std;

typedef vector <double> record_t;
typedef vector <record_t> data_t;
clock_t begin, end;
double time_spent;
//-----------------------------------------------------------------------------
// Let's overload the stream input operator to read a list of CSV fields (which a CSV record).
// Remember, a record is a list of doubles separated by spaces ' '.
istream& operator >> ( istream& ins, record_t& record )
  {
  // make sure that the returned record contains only the stuff we read now
  record.clear();

  // read the entire line into a string (a CSV record is terminated by a newline)
  string line;
  getline( ins, line );

  // now we'll use a stringstream to separate the fields out of the line
  stringstream ss( line );
  string field;
  while (getline( ss, field, ' ' ))
    {
    // for each field we wish to convert it to a double
    // (since we require that the CSV contains nothing but floating-point values)
    stringstream fs( field );
    double f = 0.0;  // (default value is 0.0)
    fs >> f;

    // add the newly-converted field to the end of the record
    record.push_back( f );
    }

  // Now we have read a single line, converted into a list of fields, converted the fields
  // from strings to doubles, and stored the results in the argument record, so
  // we just return the argument stream as required for this kind of input overload function.
  return ins;
  }

//-----------------------------------------------------------------------------
// Let's likewise overload the stream input operator to read a list of CSV records.
// This time it is a little easier, just because we only need to worry about reading
// records, and not fields.
istream& operator >> ( istream& ins, data_t& data )
  {
  // make sure that the returned data only contains the CSV data we read here
  data.clear();

  // For every record we can read from the file, append it to our resulting data
  record_t record;
  while (ins >> record)
    {
    data.push_back( record );
    }

  // Again, return the argument stream as required for this kind of input stream overload.
  return ins;  
  }

//-----------------------------------------------------------------------------
// Now to put it all to use.

int main()
  {
	  begin = clock();
  // Here is the data we want.
  data_t data;

  // Here is the file containing the data. Read it into data.
  ifstream infile( "../data/data3.csv" );
  infile >> data;
  // Complain if something went wrong.
  if (!infile.eof())
    {
    cout << "Fooey!\n";
    return 1;
    }

  infile.close();
  
// filter 

//cout << "If you want to go somewhere, GOTO is the best way to get there.\n";
  size_t i=1,change = 0,old = 0, newval=0;
  
    while (i<data.size()) {
		
	  //cout << "First, solve the problem. Then, write the code.\n";
	  
      if (fabs(data[i][2] - data[i-1][2]) < 0.0000000001) {
           //cout << "Do something unexpected, pay a bill \n";
      }
      else{
		  
		  newval = i;
		  change = newval-old;

      //cout << "The chief cause of problems is solutions.\n" ;

      for (size_t j=old;j<newval;j++) {
          data[j][2] += ((double)(16-(int)change +(int)(j-old)))*(1/(24*3600*16.0));

//cout << j << "\t" << change << "\t" << ((double)(16-(int)change +(int)(j-old)))*(1/(24*3600*16.0))<< "\t"<< data[j][2]<<"\n"; //7.2337962963×10^{-7}
      }
      old = newval;
  }
      i +=1;
  
}

for(size_t m = newval; m<data.size();m++){
	data[m][2] += ((double)(16-(int)change +(int)(m-old)))*(1/(24*3600*16.0));
}

// write file
ofstream outfile("../data/filtered_data_test.csv"); // create file
outfile << setprecision(18);
    for(unsigned int k=0;k< data.size();k++)
    {
        outfile << data[k][1] << ' ' << data[k][2]<< ' ' << data[k][3]<< ' ' << data[k][4]<< ' ' << data[k][5]<< ' ' << data[k][6]<< ' ' << data[k][7]<< ' ' << data[k][8]<< ' ' << data[k][9]<< ' ' << data[k][10]<< ' ' << data[k][11]<<"\n"; //Outputs array to txtFile
    }
    outfile.close();
//cout << "Deleted code is debugged code. \n";

// stuff
//cout<<data[5][0] <<" " << data[5][1] << " " << data[5][2] << "\n";
end = clock();
time_spent = (double)(end - begin) / CLOCKS_PER_SEC; 
cout << time_spent;
return 0;

}
