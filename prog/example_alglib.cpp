#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "interpolation.h"
#include <iostream> 

using namespace alglib;


int main(int argc, char **argv)
{
    //
    // Spline built witn spline1dbuildcubic() can be non-monotone even when
    // Y-values form monotone sequence. Say, for x=[0,1,2] and y=[0,1,1]
    // cubic spline will monotonically grow until x=1.5 and then start
    // decreasing.
    //
    // That's why ALGLIB provides special spline construction function
    // which builds spline which preserves monotonicity of the original
    // dataset.
    //
    // NOTE: in case original dataset is non-monotonic, ALGLIB splits it
    // into monotone subsequences and builds piecewise monotonic spline.
    //

// g++ example_alglib.o -o example -L../libs/ -lalglib
//g++ -Wall -O2 -I../libs/alglib/include -c example_alglib.cpp -o example_alglib.o


    real_1d_array x = "[0,1,2]";
    real_1d_array y = "[0,1,1]";
    real_2d_array tbl;
    spline1dinterpolant s;


    // build spline
    spline1dbuildmonotone(x, y, s);

    // calculate S at x = [-0.5, 0.0, 0.5, 1.0, 1.5, 2.0]
    // you may see that spline is really monotonic
    double v;
    v = spline1dcalc(s, -0.5);
    printf("%.4f\n", double(v)); // EXPECTED: 0.0000
    v = spline1dcalc(s, 0.0);
    printf("%.4f\n", double(v)); // EXPECTED: 0.0000
    v = spline1dcalc(s, +0.5);
    printf("%.4f\n", double(v)); // EXPECTED: 0.5000
    v = spline1dcalc(s, 1.0);
    printf("%.4f\n", double(v)); // EXPECTED: 1.0000
    v = spline1dcalc(s, 1.5);
    printf("%.4f\n", double(v)); // EXPECTED: 1.0000
    v = spline1dcalc(s, 2.0);
    printf("%.4f\n", double(v)); // EXPECTED: 1.0000

    ae_int_t n = 3;
   alglib::spline1dunpack(s, n, tbl);

int width = 5, height = n-2;

 for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            std::cout << tbl[i][j] << ' ';
        }
        std::cout << std::endl;
    }

    return 0;
}
