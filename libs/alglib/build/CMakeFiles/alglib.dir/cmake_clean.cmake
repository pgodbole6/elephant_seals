FILE(REMOVE_RECURSE
  "CMakeFiles/alglib.dir/src/alglibinternal.cpp.o"
  "CMakeFiles/alglib.dir/src/alglibmisc.cpp.o"
  "CMakeFiles/alglib.dir/src/ap.cpp.o"
  "CMakeFiles/alglib.dir/src/dataanalysis.cpp.o"
  "CMakeFiles/alglib.dir/src/diffequations.cpp.o"
  "CMakeFiles/alglib.dir/src/fasttransforms.cpp.o"
  "CMakeFiles/alglib.dir/src/integration.cpp.o"
  "CMakeFiles/alglib.dir/src/interpolation.cpp.o"
  "CMakeFiles/alglib.dir/src/linalg.cpp.o"
  "CMakeFiles/alglib.dir/src/optimization.cpp.o"
  "CMakeFiles/alglib.dir/src/solvers.cpp.o"
  "CMakeFiles/alglib.dir/src/specialfunctions.cpp.o"
  "CMakeFiles/alglib.dir/src/statistics.cpp.o"
  "../lib/libalglib.pdb"
  "../lib/libalglib.a"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang CXX)
  INCLUDE(CMakeFiles/alglib.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
